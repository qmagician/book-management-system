﻿namespace 图书管理系统
{
    partial class IouManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IouManage));
            this.IouFreshBtn = new System.Windows.Forms.Button();
            this.IouDeleteBtn = new System.Windows.Forms.Button();
            this.IouAddBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.IouFindBtn = new System.Windows.Forms.Button();
            this.IouFindValueText = new System.Windows.Forms.TextBox();
            this.IouFindKeyBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IouToLibraryManage = new System.Windows.Forms.Button();
            this.IouDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.IouDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // IouFreshBtn
            // 
            this.IouFreshBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IouFreshBtn.Location = new System.Drawing.Point(673, 230);
            this.IouFreshBtn.Name = "IouFreshBtn";
            this.IouFreshBtn.Size = new System.Drawing.Size(75, 31);
            this.IouFreshBtn.TabIndex = 25;
            this.IouFreshBtn.Text = "刷新";
            this.IouFreshBtn.UseVisualStyleBackColor = true;
            this.IouFreshBtn.Click += new System.EventHandler(this.IouFreshBtn_Click);
            // 
            // IouDeleteBtn
            // 
            this.IouDeleteBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IouDeleteBtn.Location = new System.Drawing.Point(673, 169);
            this.IouDeleteBtn.Name = "IouDeleteBtn";
            this.IouDeleteBtn.Size = new System.Drawing.Size(75, 31);
            this.IouDeleteBtn.TabIndex = 22;
            this.IouDeleteBtn.Text = "删除";
            this.IouDeleteBtn.UseVisualStyleBackColor = true;
            this.IouDeleteBtn.Click += new System.EventHandler(this.IouDeleteBtn_Click);
            // 
            // IouAddBtn
            // 
            this.IouAddBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IouAddBtn.Location = new System.Drawing.Point(673, 105);
            this.IouAddBtn.Name = "IouAddBtn";
            this.IouAddBtn.Size = new System.Drawing.Size(75, 31);
            this.IouAddBtn.TabIndex = 21;
            this.IouAddBtn.Text = "添加";
            this.IouAddBtn.UseVisualStyleBackColor = true;
            this.IouAddBtn.Click += new System.EventHandler(this.IouAddBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(254, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 21);
            this.label3.TabIndex = 20;
            this.label3.Text = "查找关键字：";
            // 
            // IouFindBtn
            // 
            this.IouFindBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IouFindBtn.Location = new System.Drawing.Point(577, 67);
            this.IouFindBtn.Name = "IouFindBtn";
            this.IouFindBtn.Size = new System.Drawing.Size(75, 31);
            this.IouFindBtn.TabIndex = 19;
            this.IouFindBtn.Text = "查询";
            this.IouFindBtn.UseVisualStyleBackColor = true;
            this.IouFindBtn.Click += new System.EventHandler(this.IouFindBtn_Click);
            // 
            // IouFindValueText
            // 
            this.IouFindValueText.Location = new System.Drawing.Point(390, 77);
            this.IouFindValueText.Name = "IouFindValueText";
            this.IouFindValueText.Size = new System.Drawing.Size(174, 21);
            this.IouFindValueText.TabIndex = 18;
            // 
            // IouFindKeyBox
            // 
            this.IouFindKeyBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IouFindKeyBox.FormattingEnabled = true;
            this.IouFindKeyBox.Items.AddRange(new object[] {
            "按编号查询",
            "按书名查询",
            "按账号查询",
            "按姓名查询",
            "按班级查询"});
            this.IouFindKeyBox.Location = new System.Drawing.Point(127, 77);
            this.IouFindKeyBox.Name = "IouFindKeyBox";
            this.IouFindKeyBox.Size = new System.Drawing.Size(121, 20);
            this.IouFindKeyBox.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(11, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 21);
            this.label2.TabIndex = 16;
            this.label2.Text = "查找字段：";
            // 
            // IouToLibraryManage
            // 
            this.IouToLibraryManage.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IouToLibraryManage.Location = new System.Drawing.Point(673, 409);
            this.IouToLibraryManage.Name = "IouToLibraryManage";
            this.IouToLibraryManage.Size = new System.Drawing.Size(75, 31);
            this.IouToLibraryManage.TabIndex = 15;
            this.IouToLibraryManage.Text = "返回";
            this.IouToLibraryManage.UseVisualStyleBackColor = true;
            this.IouToLibraryManage.Click += new System.EventHandler(this.IouToLibraryManage_Click);
            // 
            // IouDataGridView
            // 
            this.IouDataGridView.AllowUserToAddRows = false;
            this.IouDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.IouDataGridView.Location = new System.Drawing.Point(11, 105);
            this.IouDataGridView.Name = "IouDataGridView";
            this.IouDataGridView.RowTemplate.Height = 23;
            this.IouDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.IouDataGridView.Size = new System.Drawing.Size(641, 335);
            this.IouDataGridView.TabIndex = 14;
            this.IouDataGridView.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.IouDataGridView_RowStateChanged_1);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(144, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(377, 39);
            this.label1.TabIndex = 13;
            this.label1.Text = "欢迎进入借单管理界面";
            // 
            // IouManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::图书管理系统.Properties.Resources._1;
            this.ClientSize = new System.Drawing.Size(752, 451);
            this.Controls.Add(this.IouFreshBtn);
            this.Controls.Add(this.IouDeleteBtn);
            this.Controls.Add(this.IouAddBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.IouFindBtn);
            this.Controls.Add(this.IouFindValueText);
            this.Controls.Add(this.IouFindKeyBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IouToLibraryManage);
            this.Controls.Add(this.IouDataGridView);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(768, 490);
            this.Name = "IouManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "借单管理";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IouManage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.IouDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button IouFreshBtn;
        private System.Windows.Forms.Button IouDeleteBtn;
        private System.Windows.Forms.Button IouAddBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button IouFindBtn;
        private System.Windows.Forms.TextBox IouFindValueText;
        private System.Windows.Forms.ComboBox IouFindKeyBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button IouToLibraryManage;
        private System.Windows.Forms.DataGridView IouDataGridView;
        private System.Windows.Forms.Label label1;
    }
}