﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class AddDialog : Form
    {
        public AddDialog()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        // 点击确定添加按钮
        private void addCommitBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(addPasswordText.Text) || string.IsNullOrEmpty(addNameText.Text)
                || string.IsNullOrEmpty(addClassText.Text) || string.IsNullOrEmpty(addBookaivilableText.Text))
            {
                MessageBox.Show("请先填写完整信息", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string password = addPasswordText.Text;
            string name = addNameText.Text;
            string grade = addClassText.Text;
            string bookaivilable = addBookaivilableText.Text;

            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "insert into ReaderInfor values ("
                    + password +",'" + name + "'," + grade + "," + bookaivilable + ",0)";
                try
                {
                    database.select.ExecuteNonQuery();
                    MessageBox.Show("添加数据成功！");
                }
                finally
                {
                    database.select.Dispose();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
            this.Close();
        }
        // 取消添加
        private void addCancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
