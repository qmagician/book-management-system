﻿namespace 图书管理系统
{
    partial class AddDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.addCommitBtn = new System.Windows.Forms.Button();
            this.addCancelBtn = new System.Windows.Forms.Button();
            this.addPasswordText = new System.Windows.Forms.TextBox();
            this.addNameText = new System.Windows.Forms.TextBox();
            this.addClassText = new System.Windows.Forms.TextBox();
            this.addBookaivilableText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "密码：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(13, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "姓名：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(13, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "班级：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(13, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "可借书本书数：";
            // 
            // addCommitBtn
            // 
            this.addCommitBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.addCommitBtn.Location = new System.Drawing.Point(17, 215);
            this.addCommitBtn.Name = "addCommitBtn";
            this.addCommitBtn.Size = new System.Drawing.Size(75, 31);
            this.addCommitBtn.TabIndex = 5;
            this.addCommitBtn.Text = "确定";
            this.addCommitBtn.UseVisualStyleBackColor = true;
            this.addCommitBtn.Click += new System.EventHandler(this.addCommitBtn_Click);
            // 
            // addCancelBtn
            // 
            this.addCancelBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.addCancelBtn.Location = new System.Drawing.Point(197, 215);
            this.addCancelBtn.Name = "addCancelBtn";
            this.addCancelBtn.Size = new System.Drawing.Size(75, 31);
            this.addCancelBtn.TabIndex = 6;
            this.addCancelBtn.Text = "取消";
            this.addCancelBtn.UseVisualStyleBackColor = true;
            this.addCancelBtn.Click += new System.EventHandler(this.addCancelBtn_Click);
            // 
            // addPasswordText
            // 
            this.addPasswordText.Location = new System.Drawing.Point(80, 16);
            this.addPasswordText.Name = "addPasswordText";
            this.addPasswordText.Size = new System.Drawing.Size(192, 21);
            this.addPasswordText.TabIndex = 7;
            // 
            // addNameText
            // 
            this.addNameText.Location = new System.Drawing.Point(80, 59);
            this.addNameText.Name = "addNameText";
            this.addNameText.Size = new System.Drawing.Size(192, 21);
            this.addNameText.TabIndex = 8;
            // 
            // addClassText
            // 
            this.addClassText.Location = new System.Drawing.Point(80, 110);
            this.addClassText.Name = "addClassText";
            this.addClassText.Size = new System.Drawing.Size(192, 21);
            this.addClassText.TabIndex = 9;
            // 
            // addBookaivilableText
            // 
            this.addBookaivilableText.Location = new System.Drawing.Point(156, 153);
            this.addBookaivilableText.Name = "addBookaivilableText";
            this.addBookaivilableText.Size = new System.Drawing.Size(116, 21);
            this.addBookaivilableText.TabIndex = 10;
            // 
            // AddDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 258);
            this.Controls.Add(this.addBookaivilableText);
            this.Controls.Add(this.addClassText);
            this.Controls.Add(this.addNameText);
            this.Controls.Add(this.addPasswordText);
            this.Controls.Add(this.addCancelBtn);
            this.Controls.Add(this.addCommitBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(300, 297);
            this.MinimumSize = new System.Drawing.Size(300, 297);
            this.Name = "AddDialog";
            this.Text = "添加";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button addCommitBtn;
        private System.Windows.Forms.Button addCancelBtn;
        private System.Windows.Forms.TextBox addPasswordText;
        private System.Windows.Forms.TextBox addNameText;
        private System.Windows.Forms.TextBox addClassText;
        private System.Windows.Forms.TextBox addBookaivilableText;
    }
}