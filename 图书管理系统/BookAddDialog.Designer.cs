﻿namespace 图书管理系统
{
    partial class BookAddDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookAddDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bookISBNText = new System.Windows.Forms.TextBox();
            this.bookNameText = new System.Windows.Forms.TextBox();
            this.authorText = new System.Windows.Forms.TextBox();
            this.publishhouseText = new System.Windows.Forms.TextBox();
            this.bookTypeText = new System.Windows.Forms.TextBox();
            this.priceText = new System.Windows.Forms.TextBox();
            this.bookStockText = new System.Windows.Forms.TextBox();
            this.bookAddCommitBtn = new System.Windows.Forms.Button();
            this.bookAddCancelBtn = new System.Windows.Forms.Button();
            this.publishTimePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(60, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "编号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(59, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "书名：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(59, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "作者：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(40, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "出版社：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(59, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 21);
            this.label5.TabIndex = 4;
            this.label5.Text = "类型：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(20, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 21);
            this.label6.TabIndex = 5;
            this.label6.Text = "出版时间：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(59, 242);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 21);
            this.label7.TabIndex = 6;
            this.label7.Text = "价格：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(20, 276);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 21);
            this.label8.TabIndex = 7;
            this.label8.Text = "总库存量：";
            // 
            // bookISBNText
            // 
            this.bookISBNText.Location = new System.Drawing.Point(137, 17);
            this.bookISBNText.Name = "bookISBNText";
            this.bookISBNText.Size = new System.Drawing.Size(242, 21);
            this.bookISBNText.TabIndex = 8;
            // 
            // bookNameText
            // 
            this.bookNameText.Location = new System.Drawing.Point(137, 48);
            this.bookNameText.Name = "bookNameText";
            this.bookNameText.Size = new System.Drawing.Size(242, 21);
            this.bookNameText.TabIndex = 9;
            // 
            // authorText
            // 
            this.authorText.Location = new System.Drawing.Point(137, 88);
            this.authorText.Name = "authorText";
            this.authorText.Size = new System.Drawing.Size(242, 21);
            this.authorText.TabIndex = 10;
            // 
            // publishhouseText
            // 
            this.publishhouseText.Location = new System.Drawing.Point(137, 126);
            this.publishhouseText.Name = "publishhouseText";
            this.publishhouseText.Size = new System.Drawing.Size(242, 21);
            this.publishhouseText.TabIndex = 11;
            // 
            // bookTypeText
            // 
            this.bookTypeText.Location = new System.Drawing.Point(137, 161);
            this.bookTypeText.Name = "bookTypeText";
            this.bookTypeText.Size = new System.Drawing.Size(242, 21);
            this.bookTypeText.TabIndex = 12;
            // 
            // priceText
            // 
            this.priceText.Location = new System.Drawing.Point(137, 241);
            this.priceText.Name = "priceText";
            this.priceText.Size = new System.Drawing.Size(242, 21);
            this.priceText.TabIndex = 14;
            // 
            // bookStockText
            // 
            this.bookStockText.Location = new System.Drawing.Point(137, 276);
            this.bookStockText.Name = "bookStockText";
            this.bookStockText.Size = new System.Drawing.Size(242, 21);
            this.bookStockText.TabIndex = 15;
            // 
            // bookAddCommitBtn
            // 
            this.bookAddCommitBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookAddCommitBtn.Location = new System.Drawing.Point(54, 321);
            this.bookAddCommitBtn.Name = "bookAddCommitBtn";
            this.bookAddCommitBtn.Size = new System.Drawing.Size(75, 31);
            this.bookAddCommitBtn.TabIndex = 16;
            this.bookAddCommitBtn.Text = "确定";
            this.bookAddCommitBtn.UseVisualStyleBackColor = true;
            this.bookAddCommitBtn.Click += new System.EventHandler(this.bookAddCommitBtn_Click);
            // 
            // bookAddCancelBtn
            // 
            this.bookAddCancelBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookAddCancelBtn.Location = new System.Drawing.Point(279, 321);
            this.bookAddCancelBtn.Name = "bookAddCancelBtn";
            this.bookAddCancelBtn.Size = new System.Drawing.Size(75, 31);
            this.bookAddCancelBtn.TabIndex = 17;
            this.bookAddCancelBtn.Text = "取消";
            this.bookAddCancelBtn.UseVisualStyleBackColor = true;
            this.bookAddCancelBtn.Click += new System.EventHandler(this.bookAddCancelBtn_Click);
            // 
            // publishTimePicker
            // 
            this.publishTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.publishTimePicker.Location = new System.Drawing.Point(137, 206);
            this.publishTimePicker.Name = "publishTimePicker";
            this.publishTimePicker.Size = new System.Drawing.Size(242, 21);
            this.publishTimePicker.TabIndex = 18;
            this.publishTimePicker.Value = new System.DateTime(2017, 5, 20, 0, 0, 0, 0);
            // 
            // BookAddDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 364);
            this.Controls.Add(this.publishTimePicker);
            this.Controls.Add(this.bookAddCancelBtn);
            this.Controls.Add(this.bookAddCommitBtn);
            this.Controls.Add(this.bookStockText);
            this.Controls.Add(this.priceText);
            this.Controls.Add(this.bookTypeText);
            this.Controls.Add(this.publishhouseText);
            this.Controls.Add(this.authorText);
            this.Controls.Add(this.bookNameText);
            this.Controls.Add(this.bookISBNText);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(407, 403);
            this.MinimumSize = new System.Drawing.Size(407, 403);
            this.Name = "BookAddDialog";
            this.Text = "添加";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox bookISBNText;
        private System.Windows.Forms.TextBox bookNameText;
        private System.Windows.Forms.TextBox authorText;
        private System.Windows.Forms.TextBox publishhouseText;
        private System.Windows.Forms.TextBox bookTypeText;
        private System.Windows.Forms.TextBox priceText;
        private System.Windows.Forms.TextBox bookStockText;
        private System.Windows.Forms.Button bookAddCommitBtn;
        private System.Windows.Forms.Button bookAddCancelBtn;
        private System.Windows.Forms.DateTimePicker publishTimePicker;
    }
}