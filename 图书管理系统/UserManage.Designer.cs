﻿namespace 图书管理系统
{
    partial class UserManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserManage));
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toLibraryManage = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.findKeyBox = new System.Windows.Forms.ComboBox();
            this.findValueText = new System.Windows.Forms.TextBox();
            this.findBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.addBtn = new System.Windows.Forms.Button();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.cleanBtn = new System.Windows.Forms.Button();
            this.alterBtn = new System.Windows.Forms.Button();
            this.freshBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(145, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(377, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "欢迎进入用户管理界面";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 125);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(634, 335);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dataGridView1_RowStateChanged);
            // 
            // toLibraryManage
            // 
            this.toLibraryManage.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toLibraryManage.Location = new System.Drawing.Point(674, 429);
            this.toLibraryManage.Name = "toLibraryManage";
            this.toLibraryManage.Size = new System.Drawing.Size(75, 31);
            this.toLibraryManage.TabIndex = 2;
            this.toLibraryManage.Text = "返回";
            this.toLibraryManage.UseVisualStyleBackColor = true;
            this.toLibraryManage.Click += new System.EventHandler(this.toLibraryManage_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(12, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "查找字段：";
            // 
            // findKeyBox
            // 
            this.findKeyBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.findKeyBox.FormattingEnabled = true;
            this.findKeyBox.Items.AddRange(new object[] {
            "按账号查询",
            "按姓名查询",
            "按班级查询"});
            this.findKeyBox.Location = new System.Drawing.Point(128, 97);
            this.findKeyBox.Name = "findKeyBox";
            this.findKeyBox.Size = new System.Drawing.Size(121, 20);
            this.findKeyBox.TabIndex = 4;
            // 
            // findValueText
            // 
            this.findValueText.Location = new System.Drawing.Point(391, 97);
            this.findValueText.Name = "findValueText";
            this.findValueText.Size = new System.Drawing.Size(174, 21);
            this.findValueText.TabIndex = 5;
            // 
            // findBtn
            // 
            this.findBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.findBtn.Location = new System.Drawing.Point(571, 87);
            this.findBtn.Name = "findBtn";
            this.findBtn.Size = new System.Drawing.Size(75, 31);
            this.findBtn.TabIndex = 6;
            this.findBtn.Text = "查询";
            this.findBtn.UseVisualStyleBackColor = true;
            this.findBtn.Click += new System.EventHandler(this.findBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(255, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "查找关键字：";
            // 
            // addBtn
            // 
            this.addBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.addBtn.Location = new System.Drawing.Point(674, 125);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(75, 31);
            this.addBtn.TabIndex = 8;
            this.addBtn.Text = "添加";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // deleteBtn
            // 
            this.deleteBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.deleteBtn.Location = new System.Drawing.Point(674, 242);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(75, 31);
            this.deleteBtn.TabIndex = 9;
            this.deleteBtn.Text = "删除";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // cleanBtn
            // 
            this.cleanBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cleanBtn.Location = new System.Drawing.Point(674, 303);
            this.cleanBtn.Name = "cleanBtn";
            this.cleanBtn.Size = new System.Drawing.Size(75, 31);
            this.cleanBtn.TabIndex = 10;
            this.cleanBtn.Text = "清空";
            this.cleanBtn.UseVisualStyleBackColor = true;
            this.cleanBtn.Click += new System.EventHandler(this.cleanBtn_Click);
            // 
            // alterBtn
            // 
            this.alterBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.alterBtn.Location = new System.Drawing.Point(674, 184);
            this.alterBtn.Name = "alterBtn";
            this.alterBtn.Size = new System.Drawing.Size(75, 31);
            this.alterBtn.TabIndex = 11;
            this.alterBtn.Text = "修改";
            this.alterBtn.UseVisualStyleBackColor = true;
            this.alterBtn.Click += new System.EventHandler(this.alterBtn_Click);
            // 
            // freshBtn
            // 
            this.freshBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.freshBtn.Location = new System.Drawing.Point(674, 365);
            this.freshBtn.Name = "freshBtn";
            this.freshBtn.Size = new System.Drawing.Size(75, 31);
            this.freshBtn.TabIndex = 12;
            this.freshBtn.Text = "刷新";
            this.freshBtn.UseVisualStyleBackColor = true;
            this.freshBtn.Click += new System.EventHandler(this.freshBtn_Click);
            // 
            // UserManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::图书管理系统.Properties.Resources._31;
            this.ClientSize = new System.Drawing.Size(761, 471);
            this.Controls.Add(this.freshBtn);
            this.Controls.Add(this.alterBtn);
            this.Controls.Add(this.cleanBtn);
            this.Controls.Add(this.deleteBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.findBtn);
            this.Controls.Add(this.findValueText);
            this.Controls.Add(this.findKeyBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.toLibraryManage);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(777, 510);
            this.Name = "UserManage";
            this.Text = "用户管理";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserManage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button toLibraryManage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox findKeyBox;
        private System.Windows.Forms.TextBox findValueText;
        private System.Windows.Forms.Button findBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Button cleanBtn;
        private System.Windows.Forms.Button alterBtn;
        private System.Windows.Forms.Button freshBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}