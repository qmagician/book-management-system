﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class BookManage : Form
    {
        public BookManage()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        // 视图表显示数据
        public void showBookData()
        {
            database.select.Connection = database.connection;
            database.select.CommandText = "select * from BookInfor";
            database.adapter.SelectCommand = database.select;
            // 调用Fill方法，会自动打开数据库，并在完成后自动关闭；如果一开始是打开的，那么完成后不会关闭
            DataSet ds = new DataSet();
            database.adapter.Fill(ds);
            bookDataGridView.DataSource = ds.Tables[0];
            this.bookDataGridView.Columns[0].HeaderText = "编号";
            this.bookDataGridView.Columns[1].HeaderText = "书名";
            this.bookDataGridView.Columns[2].HeaderText = "作者名";
            this.bookDataGridView.Columns[3].HeaderText = "出版社";
            this.bookDataGridView.Columns[4].HeaderText = "类别";
            this.bookDataGridView.Columns[5].HeaderText = "出版时间";
            this.bookDataGridView.Columns[6].HeaderText = "价格";
            this.bookDataGridView.Columns[7].HeaderText = "总库存量";
            this.bookDataGridView.Columns[8].HeaderText = "剩余量";
        }
        // 显示行号
        private void bookDataGridView_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.bookDataGridView.Rows.Count; i++)
            {
                DataGridViewRow r = this.bookDataGridView.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.bookDataGridView.Refresh();
        }
        
        // 按字段进行查询
        private void bookFindBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(bookFindKeyBox.Text) || string.IsNullOrEmpty(bookFindValueText.Text))
            {
                MessageBox.Show("请先选择查找字段或填写查找关键字", "提示",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            int findKeyIndex = bookFindKeyBox.SelectedIndex;
            string findValue = bookFindValueText.Text;
            database.select.Connection = database.connection;
            switch (findKeyIndex)
            {
                case 0:
                    database.select.CommandText = "select * from BookInfor where ISBN=" + findValue;
                    break;
                case 1:
                    database.select.CommandText = "select * from BookInfor where bookname='" + findValue + "'";
                    break;
                case 2:
                    database.select.CommandText = "select * from BookInfor where author='" + findValue + "'";
                    break;
                case 3:
                    database.select.CommandText = "select * from BookInfor where publishhouse='" + findValue + "'";
                    break;
                case 4:
                    database.select.CommandText = "select * from BookInfor where booktype='" + findValue + "'";
                    break;
                case 5:
                    database.select.CommandText = "select * from BookInfor where publishtime='" + findValue + "'";
                    break;
                case 6:
                    database.select.CommandText = "select * from BookInfor where price=" + findValue;
                    break;
                default: break;
            }

            database.adapter.SelectCommand = database.select;
            DataSet ds = new DataSet();
            database.adapter.Fill(ds);
            bookDataGridView.DataSource = ds.Tables[0];
            if (bookDataGridView.Rows.Count == 0)
            {
                MessageBox.Show("抱歉！您所查找的数据不在数据库中！", "提示",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }
        }
        // 点击 “添加”按钮，显示添加对话框
        private void bookAddBtn_Click(object sender, EventArgs e)
        {
            BookAddDialog dialog = new BookAddDialog();
            dialog.ShowDialog();
        }
        // 点击 “修改”按钮，显示修改对话框
        private void bookAlterBtn_Click(object sender, EventArgs e)
        {
            if (bookDataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("请先选择您想修改的图书！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string isbn = bookDataGridView.SelectedRows[0].Cells[0].Value.ToString();
            string bookname = bookDataGridView.SelectedRows[0].Cells[1].Value.ToString();
            string author = bookDataGridView.SelectedRows[0].Cells[2].Value.ToString();
            string publishhouse = bookDataGridView.SelectedRows[0].Cells[3].Value.ToString();
            string booktype = bookDataGridView.SelectedRows[0].Cells[4].Value.ToString();
            string publishtime = bookDataGridView.SelectedRows[0].Cells[5].Value.ToString();
            string price = bookDataGridView.SelectedRows[0].Cells[6].Value.ToString();
            string stock = bookDataGridView.SelectedRows[0].Cells[7].Value.ToString();
            string surplus = bookDataGridView.SelectedRows[0].Cells[8].Value.ToString();
            BookAlterDialog dialog = new BookAlterDialog(isbn, bookname, author,
               publishhouse, booktype, publishtime, price, stock, surplus);
            dialog.ShowDialog();
        }
        // 点击 “删除”按钮
        private void bookDeleteBtn_Click(object sender, EventArgs e)
        {
            if (bookDataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("请先选择您想删除的图书！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string isbn = bookDataGridView.SelectedRows[0].Cells[0].Value.ToString();
            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "delete from BookInfor where ISBN=" + isbn;
                if (MessageBox.Show("您确定删除该图书？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        database.select.ExecuteNonQuery();
                        MessageBox.Show("删除数据成功！");
                    }
                    finally
                    {
                        database.select.Dispose();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
        }
        // 点击 “清空”按钮
        private void bookCleanBtn_Click(object sender, EventArgs e)
        {
            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "delete from BookInfor";
                if (MessageBox.Show("您确定清空该图书数据表？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        database.select.ExecuteNonQuery();
                        MessageBox.Show("图书数据表已成功清空！");
                    }
                    finally
                    {
                        database.select.Dispose();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
        }
        // 点击 “刷新”按钮
        private void bookFreshBtn_Click(object sender, EventArgs e)
        {
            bookFindKeyBox.Text = null;
            bookFindValueText.Text = null;
            showBookData();
        }

        // 点击 “返回”按钮
        private void bookToLibraryManage_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        // 当点击右上角关闭按钮时，显示主界面
        private void BookManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            LibraryManage libraryMan = new LibraryManage();
            libraryMan.Show();
        }
    }
}
