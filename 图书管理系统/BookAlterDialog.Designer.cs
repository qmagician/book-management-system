﻿namespace 图书管理系统
{
    partial class BookAlterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookAlterDialog));
            this.alterBookStockText = new System.Windows.Forms.TextBox();
            this.alterPriceText = new System.Windows.Forms.TextBox();
            this.alterBookTypeText = new System.Windows.Forms.TextBox();
            this.alterPublishhouseText = new System.Windows.Forms.TextBox();
            this.alterAuthorText = new System.Windows.Forms.TextBox();
            this.alterBookNameText = new System.Windows.Forms.TextBox();
            this.alterBookISBNText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.alterBookSurplusText = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bookAlterCancelBtn = new System.Windows.Forms.Button();
            this.bookAlterCommitBtn = new System.Windows.Forms.Button();
            this.alterDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // alterBookStockText
            // 
            this.alterBookStockText.Location = new System.Drawing.Point(137, 271);
            this.alterBookStockText.Name = "alterBookStockText";
            this.alterBookStockText.Size = new System.Drawing.Size(242, 21);
            this.alterBookStockText.TabIndex = 33;
            // 
            // alterPriceText
            // 
            this.alterPriceText.Location = new System.Drawing.Point(137, 236);
            this.alterPriceText.Name = "alterPriceText";
            this.alterPriceText.Size = new System.Drawing.Size(242, 21);
            this.alterPriceText.TabIndex = 32;
            // 
            // alterBookTypeText
            // 
            this.alterBookTypeText.Location = new System.Drawing.Point(137, 156);
            this.alterBookTypeText.Name = "alterBookTypeText";
            this.alterBookTypeText.Size = new System.Drawing.Size(242, 21);
            this.alterBookTypeText.TabIndex = 30;
            // 
            // alterPublishhouseText
            // 
            this.alterPublishhouseText.Location = new System.Drawing.Point(137, 121);
            this.alterPublishhouseText.Name = "alterPublishhouseText";
            this.alterPublishhouseText.Size = new System.Drawing.Size(242, 21);
            this.alterPublishhouseText.TabIndex = 29;
            // 
            // alterAuthorText
            // 
            this.alterAuthorText.Location = new System.Drawing.Point(137, 83);
            this.alterAuthorText.Name = "alterAuthorText";
            this.alterAuthorText.Size = new System.Drawing.Size(242, 21);
            this.alterAuthorText.TabIndex = 28;
            // 
            // alterBookNameText
            // 
            this.alterBookNameText.Location = new System.Drawing.Point(137, 43);
            this.alterBookNameText.Name = "alterBookNameText";
            this.alterBookNameText.Size = new System.Drawing.Size(242, 21);
            this.alterBookNameText.TabIndex = 27;
            // 
            // alterBookISBNText
            // 
            this.alterBookISBNText.Location = new System.Drawing.Point(137, 12);
            this.alterBookISBNText.Name = "alterBookISBNText";
            this.alterBookISBNText.Size = new System.Drawing.Size(242, 21);
            this.alterBookISBNText.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(20, 271);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 21);
            this.label8.TabIndex = 25;
            this.label8.Text = "总库存量：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(59, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 21);
            this.label7.TabIndex = 24;
            this.label7.Text = "价格：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(20, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 21);
            this.label6.TabIndex = 23;
            this.label6.Text = "出版时间：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(59, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 21);
            this.label5.TabIndex = 22;
            this.label5.Text = "类型：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(40, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 21);
            this.label4.TabIndex = 21;
            this.label4.Text = "出版社：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(59, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 21);
            this.label3.TabIndex = 20;
            this.label3.Text = "作者：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(59, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 21);
            this.label2.TabIndex = 19;
            this.label2.Text = "书名：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(60, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 21);
            this.label1.TabIndex = 18;
            this.label1.Text = "编号：";
            // 
            // alterBookSurplusText
            // 
            this.alterBookSurplusText.Location = new System.Drawing.Point(137, 307);
            this.alterBookSurplusText.Name = "alterBookSurplusText";
            this.alterBookSurplusText.Size = new System.Drawing.Size(242, 21);
            this.alterBookSurplusText.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(39, 307);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 21);
            this.label9.TabIndex = 34;
            this.label9.Text = "剩余量：";
            // 
            // bookAlterCancelBtn
            // 
            this.bookAlterCancelBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookAlterCancelBtn.Location = new System.Drawing.Point(281, 343);
            this.bookAlterCancelBtn.Name = "bookAlterCancelBtn";
            this.bookAlterCancelBtn.Size = new System.Drawing.Size(75, 31);
            this.bookAlterCancelBtn.TabIndex = 37;
            this.bookAlterCancelBtn.Text = "取消";
            this.bookAlterCancelBtn.UseVisualStyleBackColor = true;
            this.bookAlterCancelBtn.Click += new System.EventHandler(this.bookAlterCancelBtn_Click);
            // 
            // bookAlterCommitBtn
            // 
            this.bookAlterCommitBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookAlterCommitBtn.Location = new System.Drawing.Point(55, 343);
            this.bookAlterCommitBtn.Name = "bookAlterCommitBtn";
            this.bookAlterCommitBtn.Size = new System.Drawing.Size(75, 31);
            this.bookAlterCommitBtn.TabIndex = 36;
            this.bookAlterCommitBtn.Text = "确定";
            this.bookAlterCommitBtn.UseVisualStyleBackColor = true;
            this.bookAlterCommitBtn.Click += new System.EventHandler(this.bookAlterCommitBtn_Click);
            // 
            // alterDateTimePicker
            // 
            this.alterDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.alterDateTimePicker.Location = new System.Drawing.Point(137, 200);
            this.alterDateTimePicker.Name = "alterDateTimePicker";
            this.alterDateTimePicker.Size = new System.Drawing.Size(242, 21);
            this.alterDateTimePicker.TabIndex = 38;
            // 
            // BookAlterDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 381);
            this.Controls.Add(this.alterDateTimePicker);
            this.Controls.Add(this.bookAlterCancelBtn);
            this.Controls.Add(this.bookAlterCommitBtn);
            this.Controls.Add(this.alterBookSurplusText);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.alterBookStockText);
            this.Controls.Add(this.alterPriceText);
            this.Controls.Add(this.alterBookTypeText);
            this.Controls.Add(this.alterPublishhouseText);
            this.Controls.Add(this.alterAuthorText);
            this.Controls.Add(this.alterBookNameText);
            this.Controls.Add(this.alterBookISBNText);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(407, 420);
            this.MinimumSize = new System.Drawing.Size(407, 420);
            this.Name = "BookAlterDialog";
            this.Text = "修改";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox alterBookStockText;
        private System.Windows.Forms.TextBox alterPriceText;
        private System.Windows.Forms.TextBox alterBookTypeText;
        private System.Windows.Forms.TextBox alterPublishhouseText;
        private System.Windows.Forms.TextBox alterAuthorText;
        private System.Windows.Forms.TextBox alterBookNameText;
        private System.Windows.Forms.TextBox alterBookISBNText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox alterBookSurplusText;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bookAlterCancelBtn;
        private System.Windows.Forms.Button bookAlterCommitBtn;
        private System.Windows.Forms.DateTimePicker alterDateTimePicker;
    }
}