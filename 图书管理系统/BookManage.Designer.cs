﻿namespace 图书管理系统
{
    partial class BookManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookManage));
            this.label1 = new System.Windows.Forms.Label();
            this.bookFreshBtn = new System.Windows.Forms.Button();
            this.bookAlterBtn = new System.Windows.Forms.Button();
            this.bookCleanBtn = new System.Windows.Forms.Button();
            this.bookDeleteBtn = new System.Windows.Forms.Button();
            this.bookAddBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.bookFindBtn = new System.Windows.Forms.Button();
            this.bookFindValueText = new System.Windows.Forms.TextBox();
            this.bookFindKeyBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bookToLibraryManage = new System.Windows.Forms.Button();
            this.bookDataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.bookDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(313, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(377, 39);
            this.label1.TabIndex = 1;
            this.label1.Text = "欢迎进入图书管理界面";
            // 
            // bookFreshBtn
            // 
            this.bookFreshBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookFreshBtn.Location = new System.Drawing.Point(983, 357);
            this.bookFreshBtn.Name = "bookFreshBtn";
            this.bookFreshBtn.Size = new System.Drawing.Size(75, 31);
            this.bookFreshBtn.TabIndex = 24;
            this.bookFreshBtn.Text = "刷新";
            this.bookFreshBtn.UseVisualStyleBackColor = true;
            this.bookFreshBtn.Click += new System.EventHandler(this.bookFreshBtn_Click);
            // 
            // bookAlterBtn
            // 
            this.bookAlterBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookAlterBtn.Location = new System.Drawing.Point(983, 176);
            this.bookAlterBtn.Name = "bookAlterBtn";
            this.bookAlterBtn.Size = new System.Drawing.Size(75, 31);
            this.bookAlterBtn.TabIndex = 23;
            this.bookAlterBtn.Text = "修改";
            this.bookAlterBtn.UseVisualStyleBackColor = true;
            this.bookAlterBtn.Click += new System.EventHandler(this.bookAlterBtn_Click);
            // 
            // bookCleanBtn
            // 
            this.bookCleanBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookCleanBtn.Location = new System.Drawing.Point(983, 295);
            this.bookCleanBtn.Name = "bookCleanBtn";
            this.bookCleanBtn.Size = new System.Drawing.Size(75, 31);
            this.bookCleanBtn.TabIndex = 22;
            this.bookCleanBtn.Text = "清空";
            this.bookCleanBtn.UseVisualStyleBackColor = true;
            this.bookCleanBtn.Click += new System.EventHandler(this.bookCleanBtn_Click);
            // 
            // bookDeleteBtn
            // 
            this.bookDeleteBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookDeleteBtn.Location = new System.Drawing.Point(983, 234);
            this.bookDeleteBtn.Name = "bookDeleteBtn";
            this.bookDeleteBtn.Size = new System.Drawing.Size(75, 31);
            this.bookDeleteBtn.TabIndex = 21;
            this.bookDeleteBtn.Text = "删除";
            this.bookDeleteBtn.UseVisualStyleBackColor = true;
            this.bookDeleteBtn.Click += new System.EventHandler(this.bookDeleteBtn_Click);
            // 
            // bookAddBtn
            // 
            this.bookAddBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookAddBtn.Location = new System.Drawing.Point(983, 117);
            this.bookAddBtn.Name = "bookAddBtn";
            this.bookAddBtn.Size = new System.Drawing.Size(75, 31);
            this.bookAddBtn.TabIndex = 20;
            this.bookAddBtn.Text = "添加";
            this.bookAddBtn.UseVisualStyleBackColor = true;
            this.bookAddBtn.Click += new System.EventHandler(this.bookAddBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(408, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 21);
            this.label3.TabIndex = 19;
            this.label3.Text = "查找关键字：";
            // 
            // bookFindBtn
            // 
            this.bookFindBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookFindBtn.Location = new System.Drawing.Point(886, 76);
            this.bookFindBtn.Name = "bookFindBtn";
            this.bookFindBtn.Size = new System.Drawing.Size(75, 31);
            this.bookFindBtn.TabIndex = 18;
            this.bookFindBtn.Text = "查询";
            this.bookFindBtn.UseVisualStyleBackColor = true;
            this.bookFindBtn.Click += new System.EventHandler(this.bookFindBtn_Click);
            // 
            // bookFindValueText
            // 
            this.bookFindValueText.Location = new System.Drawing.Point(544, 81);
            this.bookFindValueText.Name = "bookFindValueText";
            this.bookFindValueText.Size = new System.Drawing.Size(282, 21);
            this.bookFindValueText.TabIndex = 17;
            // 
            // bookFindKeyBox
            // 
            this.bookFindKeyBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bookFindKeyBox.FormattingEnabled = true;
            this.bookFindKeyBox.Items.AddRange(new object[] {
            "按编号查询",
            "按图书名查询",
            "按作者名查询",
            "按出版社查询",
            "按类别查询",
            "按出版时间查询",
            "按价格查询"});
            this.bookFindKeyBox.Location = new System.Drawing.Point(248, 81);
            this.bookFindKeyBox.Name = "bookFindKeyBox";
            this.bookFindKeyBox.Size = new System.Drawing.Size(154, 20);
            this.bookFindKeyBox.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(119, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 21);
            this.label2.TabIndex = 15;
            this.label2.Text = "查找字段：";
            // 
            // bookToLibraryManage
            // 
            this.bookToLibraryManage.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookToLibraryManage.Location = new System.Drawing.Point(983, 421);
            this.bookToLibraryManage.Name = "bookToLibraryManage";
            this.bookToLibraryManage.Size = new System.Drawing.Size(75, 31);
            this.bookToLibraryManage.TabIndex = 14;
            this.bookToLibraryManage.Text = "返回";
            this.bookToLibraryManage.UseVisualStyleBackColor = true;
            this.bookToLibraryManage.Click += new System.EventHandler(this.bookToLibraryManage_Click);
            // 
            // bookDataGridView
            // 
            this.bookDataGridView.AllowUserToAddRows = false;
            this.bookDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bookDataGridView.Location = new System.Drawing.Point(24, 117);
            this.bookDataGridView.Name = "bookDataGridView";
            this.bookDataGridView.RowTemplate.Height = 23;
            this.bookDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.bookDataGridView.Size = new System.Drawing.Size(937, 335);
            this.bookDataGridView.TabIndex = 13;
            this.bookDataGridView.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.bookDataGridView_RowStateChanged);
            // 
            // BookManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::图书管理系统.Properties.Resources._21;
            this.ClientSize = new System.Drawing.Size(1068, 475);
            this.Controls.Add(this.bookFreshBtn);
            this.Controls.Add(this.bookAlterBtn);
            this.Controls.Add(this.bookCleanBtn);
            this.Controls.Add(this.bookDeleteBtn);
            this.Controls.Add(this.bookAddBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bookFindBtn);
            this.Controls.Add(this.bookFindValueText);
            this.Controls.Add(this.bookFindKeyBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bookToLibraryManage);
            this.Controls.Add(this.bookDataGridView);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BookManage";
            this.Text = "图书管理";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BookManage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.bookDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bookFreshBtn;
        private System.Windows.Forms.Button bookAlterBtn;
        private System.Windows.Forms.Button bookCleanBtn;
        private System.Windows.Forms.Button bookDeleteBtn;
        private System.Windows.Forms.Button bookAddBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bookFindBtn;
        private System.Windows.Forms.TextBox bookFindValueText;
        private System.Windows.Forms.ComboBox bookFindKeyBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bookToLibraryManage;
        private System.Windows.Forms.DataGridView bookDataGridView;
    }
}