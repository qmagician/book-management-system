﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class BookAddDialog : Form
    {
        public BookAddDialog()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        // 点击 “确定”按钮
        private void bookAddCommitBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(bookISBNText.Text) || string.IsNullOrEmpty(bookNameText.Text)
                || string.IsNullOrEmpty(authorText.Text) || string.IsNullOrEmpty(publishhouseText.Text)
                || string.IsNullOrEmpty(bookTypeText.Text) || string.IsNullOrEmpty(priceText.Text)
                || string.IsNullOrEmpty(bookStockText.Text))
            {
                MessageBox.Show("请先填写完整信息", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string isbn = bookISBNText.Text;
            string name = bookNameText.Text;
            string author = authorText.Text;
            string publishhouse = publishhouseText.Text;
            string booktype = bookTypeText.Text;
            string publishtime = publishTimePicker.Value.Date.ToShortDateString();
            string price = priceText.Text;
            string stock = bookStockText.Text;
            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "insert into BookInfor values(" +
                    ""+isbn+",'"+name+"','"+author+"','"+publishhouse+"','"
                    +booktype+"','"+publishtime+"',"+price+","+stock+","+stock+")";
                try
                {
                    database.select.ExecuteNonQuery();
                    MessageBox.Show("添加数据成功！");
                }
                finally
                {
                    database.select.Dispose();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
            this.Close();
        }
        // 点击 “取消”按钮
        private void bookAddCancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
