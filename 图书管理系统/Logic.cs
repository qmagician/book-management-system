﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    
    public partial class Logic : Form
    {
        public Logic()
        {
            InitializeComponent();
        }
        // 点击 登录 按钮显示主界面
        private void logicButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(accountTextBox.Text) || string.IsNullOrEmpty(passwordTextBox.Text))
            {
                MessageBox.Show("请先填写完整的登录信息！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            if (accountTextBox.Text == "manager" && passwordTextBox.Text == "123456")
            {
                // 先隐藏登录界面
                this.Visible = false;
                // 生成LibraryManage窗体实例
                LibraryManage libraryMan = new LibraryManage();              
                libraryMan.Owner = this;
                // 显示主界面
                libraryMan.Show();
            }  
            else
            {
                MessageBox.Show("账号或密码错误！请正确填写登录信息！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
        }

        public void windowShow()
        {
            // 当关闭主界面时返回登录界面，设置登录界面可见
            this.Visible = true;
        }

        // 在登录界面点击 “退出”按钮，关闭主窗口
        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        // 结束整个应用程序
        private void Logic_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
    
}
