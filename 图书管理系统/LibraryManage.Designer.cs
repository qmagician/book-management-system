﻿namespace 图书管理系统
{
    partial class LibraryManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LibraryManage));
            this.label1 = new System.Windows.Forms.Label();
            this.userManageBtn = new System.Windows.Forms.Button();
            this.bookManageBtn = new System.Windows.Forms.Button();
            this.IOUManageBtn = new System.Windows.Forms.Button();
            this.toLogicButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(513, 53);
            this.label1.TabIndex = 0;
            this.label1.Text = "欢迎进入图书管理系统";
            // 
            // userManageBtn
            // 
            this.userManageBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userManageBtn.Location = new System.Drawing.Point(21, 106);
            this.userManageBtn.Name = "userManageBtn";
            this.userManageBtn.Size = new System.Drawing.Size(164, 45);
            this.userManageBtn.TabIndex = 1;
            this.userManageBtn.Text = "用户管理系统";
            this.userManageBtn.UseVisualStyleBackColor = true;
            this.userManageBtn.Click += new System.EventHandler(this.userManageBtn_Click);
            // 
            // bookManageBtn
            // 
            this.bookManageBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookManageBtn.Location = new System.Drawing.Point(189, 158);
            this.bookManageBtn.Name = "bookManageBtn";
            this.bookManageBtn.Size = new System.Drawing.Size(164, 45);
            this.bookManageBtn.TabIndex = 2;
            this.bookManageBtn.Text = "图书管理系统";
            this.bookManageBtn.UseVisualStyleBackColor = true;
            this.bookManageBtn.Click += new System.EventHandler(this.bookManageBtn_Click);
            // 
            // IOUManageBtn
            // 
            this.IOUManageBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IOUManageBtn.Location = new System.Drawing.Point(370, 204);
            this.IOUManageBtn.Name = "IOUManageBtn";
            this.IOUManageBtn.Size = new System.Drawing.Size(164, 45);
            this.IOUManageBtn.TabIndex = 3;
            this.IOUManageBtn.Text = "借单管理系统";
            this.IOUManageBtn.UseVisualStyleBackColor = true;
            this.IOUManageBtn.Click += new System.EventHandler(this.IOUManageBtn_Click);
            // 
            // toLogicButton
            // 
            this.toLogicButton.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toLogicButton.Location = new System.Drawing.Point(21, 226);
            this.toLogicButton.Name = "toLogicButton";
            this.toLogicButton.Size = new System.Drawing.Size(74, 31);
            this.toLogicButton.TabIndex = 4;
            this.toLogicButton.Text = "返回";
            this.toLogicButton.UseVisualStyleBackColor = true;
            this.toLogicButton.Click += new System.EventHandler(this.toLogicButton_Click);
            // 
            // LibraryManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::图书管理系统.Properties.Resources._1;
            this.ClientSize = new System.Drawing.Size(537, 261);
            this.Controls.Add(this.toLogicButton);
            this.Controls.Add(this.IOUManageBtn);
            this.Controls.Add(this.bookManageBtn);
            this.Controls.Add(this.userManageBtn);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(553, 300);
            this.MinimumSize = new System.Drawing.Size(553, 300);
            this.Name = "LibraryManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "图书管理主界面";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LibraryManage_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button userManageBtn;
        private System.Windows.Forms.Button bookManageBtn;
        private System.Windows.Forms.Button IOUManageBtn;
        private System.Windows.Forms.Button toLogicButton;
    }
}