﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class IouAddDialog : Form
    {
        public IouAddDialog()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        // 确定添加
        private void IouAddBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(IouAddISBNText.Text) || string.IsNullOrEmpty(IouAddAccountText.Text)
                || string.IsNullOrEmpty(IouAddNumberText.Text))
            {
                MessageBox.Show("请先填写完整信息", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string isbn = IouAddISBNText.Text;
            string account = IouAddAccountText.Text;
            int Iounumber = int.Parse(IouAddNumberText.Text);
            database.select.Connection = database.connection;
            database.select.CommandText = "select * from BookInfor where ISBN="+isbn;
            database.adapter.SelectCommand = database.select;
            DataSet ds = new DataSet();
            database.adapter.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("抱歉！图书库中没有该编号的图书！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            if (ds.Tables[0].Rows[0][8].ToString() == "0")
            {
                MessageBox.Show("抱歉！该图书已全部借出，暂时没有剩余库存！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            if (Iounumber > int.Parse(ds.Tables[0].Rows[0][8].ToString()))
            {
                MessageBox.Show("抱歉！输入的图书数已超过剩余库存量！\n该书剩余库存量为 "+ ds.Tables[0].Rows[0][8].ToString(),
                    "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string Ioubookname = ds.Tables[0].Rows[0][1].ToString();
            string surplus = ds.Tables[0].Rows[0][8].ToString(); 

            DataSet readerds = new DataSet();
            string Iouaccount = IouAddAccountText.Text;
            database.select.CommandText = "select * from ReaderInfor where account="+Iouaccount;
            database.adapter.SelectCommand = database.select;
            database.adapter.Fill(readerds);
            
            if (readerds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("抱歉！用户库中没有该账号的用户！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            if (int.Parse(readerds.Tables[0].Rows[0][5].ToString()) == int.Parse(readerds.Tables[0].Rows[0][4].ToString()))
            {
                MessageBox.Show("抱歉！该用户的借书数目已达上限！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            
            string Iouname = readerds.Tables[0].Rows[0][2].ToString();
            string Iouclass = readerds.Tables[0].Rows[0][3].ToString();
            string bookaivilabled = readerds.Tables[0].Rows[0][5].ToString();
            
            // 更新图书库中借出图书的剩余量和用户库中已借图书的数量
            try
            {
                database.connection.Open();
                database.select.CommandText = "update BookInfor set surplus=" + (int.Parse(surplus) - Iounumber).ToString()
                    +"where ISBN="+isbn;
                database.select.ExecuteNonQuery();

                database.select.CommandText = "update ReaderInfor set bookaivilabled=" + (int.Parse(bookaivilabled) + Iounumber).ToString()
                    +"where account="+Iouaccount;
                database.select.ExecuteNonQuery();

                database.select.CommandText = "insert into IouInfor values("+isbn+",'"+ Ioubookname+"',"+ Iouaccount
                    +",'"+ Iouname+"','"+Iouclass+"',"+Iounumber+")";
                database.select.ExecuteNonQuery();
                MessageBox.Show("添加借单成功！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常："+ex.Message, "错误！", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return;
            }
            finally
            {
                database.select.Dispose();
                database.connection.Close();
            }
            this.Close();
        }
        // 取消添加
        private void IouAddCancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
