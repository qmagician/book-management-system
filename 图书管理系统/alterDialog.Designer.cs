﻿namespace 图书管理系统
{
    partial class AlterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlterDialog));
            this.alterBookaivilableText = new System.Windows.Forms.TextBox();
            this.alterClassText = new System.Windows.Forms.TextBox();
            this.alterNameText = new System.Windows.Forms.TextBox();
            this.alterPasswordText = new System.Windows.Forms.TextBox();
            this.alterCancelBtn = new System.Windows.Forms.Button();
            this.alterCommitBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.alterBookaivilabledText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.alterAccountText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // alterBookaivilableText
            // 
            this.alterBookaivilableText.Location = new System.Drawing.Point(134, 166);
            this.alterBookaivilableText.Name = "alterBookaivilableText";
            this.alterBookaivilableText.Size = new System.Drawing.Size(137, 21);
            this.alterBookaivilableText.TabIndex = 20;
            // 
            // alterClassText
            // 
            this.alterClassText.Location = new System.Drawing.Point(80, 130);
            this.alterClassText.Name = "alterClassText";
            this.alterClassText.Size = new System.Drawing.Size(192, 21);
            this.alterClassText.TabIndex = 19;
            // 
            // alterNameText
            // 
            this.alterNameText.Location = new System.Drawing.Point(80, 88);
            this.alterNameText.Name = "alterNameText";
            this.alterNameText.Size = new System.Drawing.Size(192, 21);
            this.alterNameText.TabIndex = 18;
            // 
            // alterPasswordText
            // 
            this.alterPasswordText.Location = new System.Drawing.Point(80, 51);
            this.alterPasswordText.Name = "alterPasswordText";
            this.alterPasswordText.Size = new System.Drawing.Size(192, 21);
            this.alterPasswordText.TabIndex = 17;
            // 
            // alterCancelBtn
            // 
            this.alterCancelBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.alterCancelBtn.Location = new System.Drawing.Point(197, 260);
            this.alterCancelBtn.Name = "alterCancelBtn";
            this.alterCancelBtn.Size = new System.Drawing.Size(75, 31);
            this.alterCancelBtn.TabIndex = 16;
            this.alterCancelBtn.Text = "取消";
            this.alterCancelBtn.UseVisualStyleBackColor = true;
            this.alterCancelBtn.Click += new System.EventHandler(this.alterCancelBtn_Click);
            // 
            // alterCommitBtn
            // 
            this.alterCommitBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.alterCommitBtn.Location = new System.Drawing.Point(17, 260);
            this.alterCommitBtn.Name = "alterCommitBtn";
            this.alterCommitBtn.Size = new System.Drawing.Size(75, 31);
            this.alterCommitBtn.TabIndex = 15;
            this.alterCommitBtn.Text = "确定";
            this.alterCommitBtn.UseVisualStyleBackColor = true;
            this.alterCommitBtn.Click += new System.EventHandler(this.alterCommitBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(12, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 21);
            this.label4.TabIndex = 14;
            this.label4.Text = "可借书本数：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(13, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 21);
            this.label3.TabIndex = 13;
            this.label3.Text = "班级：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(13, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 21);
            this.label2.TabIndex = 12;
            this.label2.Text = "姓名：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(13, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 21);
            this.label1.TabIndex = 11;
            this.label1.Text = "密码：";
            // 
            // alterBookaivilabledText
            // 
            this.alterBookaivilabledText.Location = new System.Drawing.Point(135, 205);
            this.alterBookaivilabledText.Name = "alterBookaivilabledText";
            this.alterBookaivilabledText.Size = new System.Drawing.Size(137, 21);
            this.alterBookaivilabledText.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(13, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 21);
            this.label5.TabIndex = 21;
            this.label5.Text = "已借书本数：";
            // 
            // alterAccountText
            // 
            this.alterAccountText.Location = new System.Drawing.Point(80, 12);
            this.alterAccountText.Name = "alterAccountText";
            this.alterAccountText.ReadOnly = true;
            this.alterAccountText.Size = new System.Drawing.Size(192, 21);
            this.alterAccountText.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(13, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 21);
            this.label6.TabIndex = 23;
            this.label6.Text = "账号：";
            // 
            // AlterDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 311);
            this.Controls.Add(this.alterAccountText);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.alterBookaivilabledText);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.alterBookaivilableText);
            this.Controls.Add(this.alterClassText);
            this.Controls.Add(this.alterNameText);
            this.Controls.Add(this.alterPasswordText);
            this.Controls.Add(this.alterCancelBtn);
            this.Controls.Add(this.alterCommitBtn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(300, 350);
            this.MinimumSize = new System.Drawing.Size(300, 350);
            this.Name = "AlterDialog";
            this.Text = "修改";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button alterCancelBtn;
        private System.Windows.Forms.Button alterCommitBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox alterPasswordText;
        public System.Windows.Forms.TextBox alterAccountText;
        public System.Windows.Forms.TextBox alterBookaivilableText;
        public System.Windows.Forms.TextBox alterClassText;
        public System.Windows.Forms.TextBox alterNameText;
        public System.Windows.Forms.TextBox alterBookaivilabledText;
    }
}