﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class IouManage : Form
    {
        public IouManage()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        // 视图显示数据
        public  void IoushowData()
        {
            database.select.Connection = database.connection;
            database.select.CommandText = "select * from IouInfor";
            database.adapter.SelectCommand = database.select;
            // 调用Fill方法，会自动打开数据库，并在完成后自动关闭；如果一开始是打开的，那么完成后不会关闭
            DataSet ds = new DataSet();
            database.adapter.Fill(ds);
            IouDataGridView.DataSource = ds.Tables[0];
            this.IouDataGridView.Columns[0].HeaderText = "编号";
            this.IouDataGridView.Columns[1].HeaderText = "书名";
            this.IouDataGridView.Columns[2].HeaderText = "借阅者账号";
            this.IouDataGridView.Columns[3].HeaderText = "借阅者姓名";
            this.IouDataGridView.Columns[4].HeaderText = "借阅者班别";
            this.IouDataGridView.Columns[5].HeaderText = "所借数目";
        }
        // 显示行号
        private void IouDataGridView_RowStateChanged_1(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.IouDataGridView.Rows.Count; i++)
            {
                DataGridViewRow r = this.IouDataGridView.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.IouDataGridView.Refresh();
        }

        // 查询
        private void IouFindBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(IouFindKeyBox.Text) || string.IsNullOrEmpty(IouFindValueText.Text))
            {
                MessageBox.Show("请先选择查找字段或填写查找关键字", "提示",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            int findKeyIndex = IouFindKeyBox.SelectedIndex;
            string findValue = IouFindValueText.Text;
            database.select.Connection = database.connection;
            switch (findKeyIndex)
            {
                case 0:
                    database.select.CommandText = "select * from IouInfor where IouISBN=" + findValue;
                    break;
                case 1:
                    database.select.CommandText = "select * from IouInfor where Ioubookname='" + findValue +"'";
                    break;
                case 2:
                    database.select.CommandText = "select * from IouInfor where Iouaccount=" + findValue;
                    break;
                case 3:
                    database.select.CommandText = "select * from IouInfor where Iouname='" + findValue +"'";
                    break;
                case 4:
                    database.select.CommandText = "select * from IouInfor where Iouclass='" + findValue+"'";
                    break;
                default:break;
            }
            database.adapter.SelectCommand = database.select;
            DataSet ds = new DataSet();
            database.adapter.Fill(ds);
            IouDataGridView.DataSource = ds.Tables[0];
            if (IouDataGridView.Rows.Count == 0)
            {
                MessageBox.Show("抱歉！您所查找的数据不在数据库中！", "提示",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }
        }
        // 添加
        private void IouAddBtn_Click(object sender, EventArgs e)
        {
            IouAddDialog dialog = new IouAddDialog();
            dialog.ShowDialog();
        }

        // 删除
        private void IouDeleteBtn_Click(object sender, EventArgs e)
        {
            if (IouDataGridView.SelectedRows.Count == 0)
            {
                MessageBox.Show("请先选择您想删除的借单！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            if (MessageBox.Show("您确定删除该借单？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                // 更新图书库已还图书的剩余量
                string isbn = IouDataGridView.SelectedRows[0].Cells[0].Value.ToString();
                int Iousurplus = int.Parse(IouDataGridView.SelectedRows[0].Cells[5].Value.ToString());
                database.select.Connection = database.connection;
                database.select.CommandText = "select * from BookInfor where ISBN=" + isbn;
                database.adapter.SelectCommand = database.select;
                DataSet ds = new DataSet();
                database.adapter.Fill(ds);

                int surplus = int.Parse(ds.Tables[0].Rows[0][8].ToString());
                try
                {
                    database.connection.Open();
                    database.select.CommandText = "update BookInfor set surplus=" + (surplus + Iousurplus).ToString() + " where ISBN=" + isbn;
                    database.select.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("出现异常：" + ex.Message, "错误！", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    return;
                }
                // 减少用户库中已借书的数量
                string account = IouDataGridView.SelectedRows[0].Cells[2].Value.ToString();
                database.select.CommandText = "select * from ReaderInfor where account=" + account;
                DataSet readerds = new DataSet();
                database.adapter.Fill(readerds);
                int bookaivilabled = int.Parse(readerds.Tables[0].Rows[0][5].ToString());
                try
                {
                    database.select.CommandText = "update ReaderInfor set bookaivilabled=" + (bookaivilabled - Iousurplus).ToString() + " where account=" + account;
                    database.select.ExecuteNonQuery();
                    database.select.CommandText = "delete from IouInfor where IouISBN=" + isbn+"and Iouaccount="+account;
                    database.select.ExecuteNonQuery();
                    MessageBox.Show("借单删除成功！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("出现异常：" + ex.Message, "错误！", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    return;
                }
                finally
                {
                    database.select.Dispose();
                    database.connection.Close();
                }
            }
        }
        // 刷新
        private void IouFreshBtn_Click(object sender, EventArgs e)
        {
            IoushowData();
        }
        // 返回
        private void IouToLibraryManage_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        // 点击右上角关闭按钮
        private void IouManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            LibraryManage libraryMan = new LibraryManage();
            libraryMan.Show();
        }
    }
}
