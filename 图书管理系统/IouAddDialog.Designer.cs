﻿namespace 图书管理系统
{
    partial class IouAddDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IouAddDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.IouAddISBNText = new System.Windows.Forms.TextBox();
            this.IouAddAccountText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.IouAddNumberText = new System.Windows.Forms.TextBox();
            this.IouAddBtn = new System.Windows.Forms.Button();
            this.IouAddCancelBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(12, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "编号:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(12, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "账号：";
            // 
            // IouAddISBNText
            // 
            this.IouAddISBNText.Location = new System.Drawing.Point(86, 150);
            this.IouAddISBNText.Name = "IouAddISBNText";
            this.IouAddISBNText.Size = new System.Drawing.Size(246, 21);
            this.IouAddISBNText.TabIndex = 6;
            // 
            // IouAddAccountText
            // 
            this.IouAddAccountText.Location = new System.Drawing.Point(86, 189);
            this.IouAddAccountText.Name = "IouAddAccountText";
            this.IouAddAccountText.Size = new System.Drawing.Size(246, 21);
            this.IouAddAccountText.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(12, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 21);
            this.label2.TabIndex = 8;
            this.label2.Text = "数量：";
            // 
            // IouAddNumberText
            // 
            this.IouAddNumberText.Location = new System.Drawing.Point(86, 228);
            this.IouAddNumberText.Name = "IouAddNumberText";
            this.IouAddNumberText.Size = new System.Drawing.Size(246, 21);
            this.IouAddNumberText.TabIndex = 9;
            // 
            // IouAddBtn
            // 
            this.IouAddBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IouAddBtn.Location = new System.Drawing.Point(16, 262);
            this.IouAddBtn.Name = "IouAddBtn";
            this.IouAddBtn.Size = new System.Drawing.Size(75, 31);
            this.IouAddBtn.TabIndex = 10;
            this.IouAddBtn.Text = "确定";
            this.IouAddBtn.UseVisualStyleBackColor = true;
            this.IouAddBtn.Click += new System.EventHandler(this.IouAddBtn_Click);
            // 
            // IouAddCancelBtn
            // 
            this.IouAddCancelBtn.Font = new System.Drawing.Font("华文楷体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.IouAddCancelBtn.Location = new System.Drawing.Point(257, 262);
            this.IouAddCancelBtn.Name = "IouAddCancelBtn";
            this.IouAddCancelBtn.Size = new System.Drawing.Size(75, 31);
            this.IouAddCancelBtn.TabIndex = 11;
            this.IouAddCancelBtn.Text = "取消";
            this.IouAddCancelBtn.UseVisualStyleBackColor = true;
            this.IouAddCancelBtn.Click += new System.EventHandler(this.IouAddCancelBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("华文楷体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(135, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 18);
            this.label4.TabIndex = 12;
            this.label4.Text = "温馨提示！";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox1.Font = new System.Drawing.Font("华文楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(16, 40);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(319, 79);
            this.textBox1.TabIndex = 13;
            this.textBox1.Text = "为避免数据的错乱，请在借书时不要对同一个账号借同一本编号的书进行多次添加操作！若想修改可直接将原有借单删除，再重新添加！";
            // 
            // IouAddDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 305);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IouAddCancelBtn);
            this.Controls.Add(this.IouAddBtn);
            this.Controls.Add(this.IouAddNumberText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IouAddAccountText);
            this.Controls.Add(this.IouAddISBNText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(360, 344);
            this.Name = "IouAddDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "添加";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox IouAddISBNText;
        private System.Windows.Forms.TextBox IouAddAccountText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox IouAddNumberText;
        private System.Windows.Forms.Button IouAddBtn;
        private System.Windows.Forms.Button IouAddCancelBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
    }
}