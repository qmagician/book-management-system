﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class LibraryManage : Form
    {
        
        public LibraryManage()
        {
            InitializeComponent();
        }
        Logic logic = new Logic();
        // 点击 “用户管理系统”按钮
        private void userManageBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            logic.Visible = false;
            UserManage userMan = new UserManage();
            userMan.Show();

            userMan.showData();
        }
        // 点击 “图书管理系统”按钮
        private void bookManageBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            logic.Visible = false;
            BookManage bookMan = new BookManage();
            bookMan.Show();

            bookMan.showBookData();
        }
        // 点击 “借单管理系统”按钮
        private void IOUManageBtn_Click(object sender, EventArgs e)
        {
            this.Close();
            logic.Visible = false;
            IouManage IouMan = new IouManage();
            IouMan.Show();

            IouMan.IoushowData();
        }
        // 点击右上角关闭按钮时，显示  
        private void LibraryManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            logic.windowShow();
        }
        // 点击 “返回”按钮时，关闭主界面，调用登录界面显示函数
        private void toLogicButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
