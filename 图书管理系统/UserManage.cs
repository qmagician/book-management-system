﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace 图书管理系统
{
    public partial class UserManage : Form
    {
        public UserManage()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        // 视图表显示数据
        public void showData()
        {
            database.select.Connection = database.connection;
            database.select.CommandText = "select * from ReaderInfor";
            database.adapter.SelectCommand = database.select;
            // 调用Fill方法，会自动打开数据库，并在完成后自动关闭；如果一开始是打开的，那么完成后不会关闭
            DataSet ds = new DataSet();
            database.adapter.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            this.dataGridView1.Columns[0].HeaderText = "账号";
            this.dataGridView1.Columns[1].HeaderText = "密码";
            this.dataGridView1.Columns[2].HeaderText = "姓名";
            this.dataGridView1.Columns[3].HeaderText = "班别";
            this.dataGridView1.Columns[4].HeaderText = "可借书本数";
            this.dataGridView1.Columns[5].HeaderText = "已借书本数";
        }
        // 显示行号
        private void dataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //显示在HeaderCell上
            for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
            {
                DataGridViewRow r = this.dataGridView1.Rows[i];
                r.HeaderCell.Value = string.Format("{0}", i + 1);
            }
            this.dataGridView1.Refresh();
        }
        
        // 按字段进行查询
        private void findBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(findKeyBox.Text) || string.IsNullOrEmpty(findValueText.Text))
            {
                MessageBox.Show("请先选择查找字段或填写查找关键字", "提示", 
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            int findKeyIndex = findKeyBox.SelectedIndex;
            string findValue = findValueText.Text;
            database.select.Connection = database.connection;
            switch (findKeyIndex)
            {
                case 0:
                    database.select.CommandText = "select * from ReaderInfor where account=" + findValue;
                    break;
                case 1:
                    database.select.CommandText = "select * from ReaderInfor where name='" + findValue+"'";
                    break;
                case 2:
                    database.select.CommandText = "select * from ReaderInfor where class=" + findValue;
                    break;
                default:break;
            }
            
            database.adapter.SelectCommand = database.select;
            DataSet ds = new DataSet();
            database.adapter.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("抱歉！您所查找的数据不在数据库中！", "提示",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }
        }
        // 添加
        private void addBtn_Click(object sender, EventArgs e)
        {
            AddDialog dialog = new AddDialog();
            dialog.ShowDialog();
        }
        // 修改
        private void alterBtn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("请先选择您想修改的用户！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string userAcount = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            string userPassword = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string userName = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            string userClass = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            string userBookaivilable = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            string userBookaivilabled = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            
            AlterDialog dialog = new AlterDialog(userAcount, userPassword, userName,
               userClass, userBookaivilable, userBookaivilabled);
            dialog.ShowDialog();       
        }
        // 删除
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("请先选择您想删除的用户！", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string account = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "delete from ReaderInfor where account=" + account;
                if (MessageBox.Show("您确定删除该用户？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        database.select.ExecuteNonQuery();
                        MessageBox.Show("删除数据成功！");
                    }
                    finally
                    {
                        database.select.Dispose();
                    }
                }
                else
                {
                    return;
                }              
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
        }
        // 清空
        private void cleanBtn_Click(object sender, EventArgs e)
        {
            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "delete from ReaderInfor";
                if (MessageBox.Show("您确定清空该用户数据表？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        database.select.ExecuteNonQuery();
                        MessageBox.Show("用户数据表已成功清空！");
                    }
                    finally
                    {
                        database.select.Dispose();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
        }
        // 刷新
        private void freshBtn_Click(object sender, EventArgs e)
        {
            findKeyBox.Text = null;
            findValueText.Text = null;
            showData();
        }
       
        // 当点击右上角关闭按钮时，显示主界面
        private void UserManage_FormClosing(object sender, FormClosingEventArgs e)
        {
            LibraryManage libraryMan = new LibraryManage();
            libraryMan.Show();
        }
        // 点击 “返回”按钮
        private void toLibraryManage_Click(object sender, EventArgs e)
        {
            this.Close(); //事件机制会执行UserManage_FormClosing事件
        }
    }
}
