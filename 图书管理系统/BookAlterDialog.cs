﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class BookAlterDialog : Form
    {
        public BookAlterDialog()
        {
            InitializeComponent();
        }
        public BookAlterDialog(string isbn, string bookname, string author, string publishhouse,
            string booktype, string publishtime, string price, string stock, string surplus)
        {
            InitializeComponent();
            alterBookISBNText.Text = isbn;
            alterBookNameText.Text = bookname;
            alterAuthorText.Text = author;
            alterPublishhouseText.Text = publishhouse;
            alterBookTypeText.Text = booktype;
            alterDateTimePicker.Text = publishtime;
            alterPriceText.Text = price;
            alterBookStockText.Text = stock;
            alterBookSurplusText.Text = surplus;
        }
        DataBase database = new DataBase();
        // 点击 “确定”按钮
        private void bookAlterCommitBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(alterBookISBNText.Text) || string.IsNullOrEmpty(alterBookNameText.Text)
                || string.IsNullOrEmpty(alterAuthorText.Text) || string.IsNullOrEmpty(alterPublishhouseText.Text)
                || string.IsNullOrEmpty(alterBookTypeText.Text) || string.IsNullOrEmpty(alterPriceText.Text)
                || string.IsNullOrEmpty(alterBookStockText.Text) || string.IsNullOrEmpty(alterBookSurplusText.Text))
            {
                MessageBox.Show("请先填写完整信息", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string isbn = alterBookISBNText.Text;
            string booknam = alterBookNameText.Text;
            string author = alterAuthorText.Text;
            string publishhouse = alterPublishhouseText.Text;
            string booktype = alterBookTypeText.Text;
            string publishtime = alterDateTimePicker.Text;
            string price = alterPriceText.Text;
            string stock = alterBookStockText.Text;
            string surplus = alterBookSurplusText.Text;
            
            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "update BookInfor set ISBN=" + isbn + ",bookname='" +
                    booknam + "',author='" + author + "',publishhouse='" + publishhouse + "',booktype='" + booktype +
                    "',publishtime='" + publishtime + "',price=" + price + ",stock=" + stock + ",surplus=" + surplus +
                    "where ISBN=" + isbn;
                    
                try
                {
                    database.select.ExecuteNonQuery();
                    MessageBox.Show("修改数据成功！");
                }
                finally
                {
                    database.select.Dispose();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
            this.Close();
        }
        // 点击 “取消”按钮
        private void bookAlterCancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
