﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 图书管理系统
{
    public partial class AlterDialog : Form
    {
        public AlterDialog()
        {
            InitializeComponent();
        }
        public AlterDialog(string account, string password, string name, 
            string grade, string bookaivilable, string bookaivilabled)
        {
            InitializeComponent();

            alterAccountText.Text = account;
            alterPasswordText.Text = password;
            alterNameText.Text = name;
            alterClassText.Text = grade;
            alterBookaivilableText.Text = bookaivilable;
            alterBookaivilabledText.Text = bookaivilabled;
        }
        DataBase database = new DataBase();
        // 确定修改
        private void alterCommitBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(alterPasswordText.Text) || string.IsNullOrEmpty(alterNameText.Text)
                || string.IsNullOrEmpty(alterClassText.Text) || string.IsNullOrEmpty(alterBookaivilableText.Text)
                || string.IsNullOrEmpty(alterBookaivilabledText.Text))
            {
                MessageBox.Show("请先填写完整信息", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                return;
            }
            string account = alterAccountText.Text;
            string password = alterPasswordText.Text;
            string name = alterNameText.Text;
            string grade = alterClassText.Text;
            string bookaivilable = alterBookaivilableText.Text;
            string bookaivilabled = alterBookaivilabledText.Text;

            try
            {
                database.connection.Open();
                database.select.Connection = database.connection;
                database.select.CommandType = CommandType.Text;
                database.select.CommandText = "update ReaderInfor set password="
                    + password + ",name='" + name + "',class=" + grade + ",bookaivilable="
                    + bookaivilable + ",bookaivilabled=" + bookaivilabled + "where account=" + account;
                try
                {
                    database.select.ExecuteNonQuery();
                    MessageBox.Show("修改数据成功！");
                }
                finally
                {
                    database.select.Dispose();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("出现异常:" + ex.Message, "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            finally
            {
                database.connection.Close();
            }
            this.Close();
        }

        // 取消修改
        private void alterCancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }       
    }
}
